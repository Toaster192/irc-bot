"""Inter-process message queue."""
from hotqueue import HotQueue

IRC_PUBLISH_QUEUE = HotQueue("irc-publish", host='localhost')
